---
title: "Web Hacking"
date: 2022-10-23T00:00:00+00:00
author: PHCT
image_webp: images/blog/material/u3_web_hacking.webp
image: images/blog/material/u3_web_hacking.webp
description : "Unidad 3. Web Hacking."
categories: ["Material didactico"]
tags: ["web", "sqli", "xss", "owasp", "autenticacion"]
---

## Web Hacking

Las empresas y organizaciones ofrecen sus productos y servicios principalmente a través de servidores web. Existen vulnerabilidades comunes (vease el OWASP Top Ten) que se pueden utilizar para comprometer a toda una organización a través de los servicios que abren al público, como lo son las inyecciones SQL, malas configuraciones de autenticación, cross-site scripting (XSS), entre otras.

### Material didáctico

[Web Hacking](https://gitlab.com/PumaHat/pumahat.gitlab.io/-/tree/main/content/english/material/U3_Web_Hacking)