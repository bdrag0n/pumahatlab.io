---
title: "Simple CTF"
date: 2018-09-12T14:51:12+06:00
author: pCix
image_webp: images/blog/pCix/simpleCTF/simpleCTF.png
image: images/blog/pCix/simpleCTF/simpleCTF.png
categories: ["Writeups"]
tags: ["Pentesting","TryHackMe"]
---
# Simple CTF
## Configuración inicial

Recordemos que como primer paso debemos descargar nuestro archivo con extensión *.ovpn* para poder acceder a las máquinas de la plataforma [*Try Hack Me*.](https://tryhackme.com) Ya que contamos con nuestro archivo, lo utilizaremos para conectarnos por _VPN_ a las máquinas. Una conexión _VPN_ crea una conexión entre nuestra computadora y la red local en la que se encuentra la máquina, de este modo podemos interactuar con ella como si fueramos parte de esa red local (es por eso que al crear la conexión se asigna una dirección IP a nuestra computadora). 

Ejecutamos el siguiente comando para conectarnos por _VPN_:

![vpn](/images/blog/pCix/simpleCTF/_resources/d5c77957240c83eb33ae1b0ca9d7cf34.png)

La IP que nos asigna la conexión _VPN_ la podemos revisar en otra consola con el comando `ifconfig` y  en este caso la visualizamos en la interfaz `tun0`:

![ifconfig](/images/blog/pCix/simpleCTF/_resources/9e2c48d1e01d8a554e65632e3aada3a9.png)

Iniciamos la máquina y obtenemos la dirección IP de la misma:

![ip](/images/blog/pCix/simpleCTF/_resources/bf590b98a9a7fa77df1ce2d48781f61e.png)

## Escaneo de puertos

Dada la IP: _10.10.108.27_ proporcionada por [*Try Hack Me*.](https://tryhackme.com) utilizamos el siguiente comando para hacer un escaneo de puertos:
```
sudo nmap -A 10.10.108.27
```
La bandera `-A` es utilizada principalmente para habilitar la detección de sistemas operativos y versiones. Esto nos dará más información que un escaneo sin esta bandera, sin embargo también tomará más tiempo.

El resultado fue el siguiente: 

```
┌──(kali㉿kali)-[~]
└─$ sudo nmap -A 10.10.108.27 
[sudo] password for kali: 
Starting Nmap 7.91 ( https://nmap.org ) at 2022-01-27 19:32 EST
Nmap scan report for 10.10.108.27
Host is up (0.20s latency).
Not shown: 997 filtered ports
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: TIMEOUT
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:10.2.101.155
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 4
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-robots.txt: 2 disallowed entries 
|_/ /openemr-5_0_1_3 
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
2222/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 29:42:69:14:9e:ca:d9:17:98:8c:27:72:3a:cd:a9:23 (RSA)
|   256 9b:d1:65:07:51:08:00:61:98:de:95:ed:3a:e3:81:1c (ECDSA)
|_  256 12:65:1b:61:cf:4d:e5:75:fe:f4:e8:d4:6e:10:2a:f6 (ED25519)
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: Linux 3.10 - 3.13 (91%), Crestron XPanel control system (89%), HP P2000 G3 NAS device (86%), ASUS RT-N56U WAP (Linux 3.4) (86%), Linux 3.1 (86%), Linux 3.16 (86%), Linux 3.2 (86%), AXIS 210A or 211 Network Camera (Linux 2.6.17) (86%), Linux 2.6.32 (85%), Linux 2.6.32 - 3.1 (85%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 4 hops
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 80/tcp)
HOP RTT       ADDRESS
1   57.01 ms  10.2.0.1
2   ... 3
4   197.76 ms 10.10.108.27

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 62.23 seconds
                                                                   
```

### FTP (File Transfer Protocol)
La herramienta _nmap_ nos indicó que cuenta con el servicio _ftp_ en el puerto 22. Además dice que permite hacer _login_ con _Anonymous_ en el servidor. Así que se procede a conectarse:

![ftp](/images/blog/pCix/simpleCTF/_resources/c788eb49cd57d5820afcd8a87bc28497.png)

En el servicio _ftp_ se encontró un archivo siguiendo los siguientes comandos:
```
┌──(kali㉿kali)-[~]
└─$ ftp 10.10.108.27
Connected to 10.10.108.27.
220 (vsFTPd 3.0.3)
Name (10.10.108.27:kali): Anonymous
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
drwxr-xr-x    2 ftp      ftp          4096 Aug 17  2019 pub
226 Directory send OK.
ftp> cd pub
250 Directory successfully changed.
ftp> ls
200 PORT command successful. Consider using PASV.
150 Here comes the directory listing.
-rw-r--r--    1 ftp      ftp           166 Aug 17  2019 ForMitch.txt
226 Directory send OK.
ftp> get ForMitch.txt
local: ForMitch.txt remote: ForMitch.txt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for ForMitch.txt (166 bytes).
226 Transfer complete.
166 bytes received in 0.00 secs (72.5971 kB/s)
ftp> exit
221 Goodbye.
                                                                                                  
┌──(kali㉿kali)-[~]
└─$ cat ForMitch.txt 
Dammit man... you'te the worst dev i've seen. You set the same pass for the system user, and the password is so weak... i cracked it in seconds. Gosh... what a mess!

```

Gracias al _ftp_ ahora sabemos que hay una contraseña fácil de crackear. Por lo mientras seguiremos explorando la máquina.

### Servicio HTTP
La herramienta _nmap_ nos indicó que tiene el puerto 80 abierto y corre un servicio _http_ por lo que se verifica que haya una página web en la IP de la máquina. La página mostrada es la siguiente:

![http](/images/blog/pCix/simpleCTF/_resources/fa2e42da60e3262a72743b2886ec011f.png)

Como no hemos obtenido suficiente información para pensar en un posible ataque se hará uso de la herramienta [_dirbuster_](https://www.kali.org/tools/dirbuster/) para hacer _Brute Force_ de directorios y archivos sobre la página web. Esto con la finalidad de encontrar alguna carpeta o archivo que sea de nuestro interés. La configuración que se utilizó fue la siguiente:

![dirbuster](/images/blog/pCix/simpleCTF/_resources/5832894872510fa00a90c86a9b143615.png)

Después de un tiempo de ejecución se mostró un directorio interesante (El directorio `/simple`):

![dirbusterResultado](/images/blog/pCix/simpleCTF/_resources/734a1c908b54a8d7511cbc98cf098340.png)

### Exploit
En la página principal que muestra el directorio `/simple` encontramos un `cms madesimple  version 2.2.8`.

Al interactuar más con la página encontramos que hay un login en la carpeta  `/admin`. Así que procedemos a buscar más acerca del _cms madesimple_ y tratar de encontrar algún exploit. 
 
Se encontró un exploit en: https://www.exploit-db.com/exploits/46635 que es para versiones menores a 2.2.10 de _cms madesimple_. Este exploit es para realizar un _sql injection_  que se encarga de obtener información de la base de datos de la página. El único detalle es que está esrito en _python 2_. Como primer intento tratamos de hacerlo funcionar con _python 3_ modificando las funciones `print` agregando los paréntesis `()` que son necesarios en _python 3_.

Una vez que se hicieron las modificaciones en el exploit se utilizó el siguiente comando para ejecutar el exploit tal cual se muestra en el ejemplo del exploit:
`python3 exploiy.py -u http://10.10.108.27/simple`

El resultado fue el siguiente:

![exploit](/images/blog/pCix/simpleCTF/_resources/cd06f1a78742aca46da6abc345871fe6.png)

**Concepto básico de función _hash_**:  Una función _hash_ es una función que toma como entrada cualquier cantidad de datos y se encarga de transformarlos en una salida de tamaño fijo. Una función _hash_ pretende ser sólo de ida, esto quiere decir que cuando a ciertos datos de le aplica una función _hash_ es fácil ir pero ya no debe de ser reversible, es decir, ya no debe de ser intuitivo regresar.

Recordemos que en una base de datos en lugar de guardar las contraseñas en texto claro se guardan los resultados de aplicarles una función _hash()_. En este caso contamos con el resultado de esa función _hash_. 
Primero debemos encontrar el posible tipo de algoritmo que fue utilizado. Una herramienta para descubrir el posible algoritmo es _hash-identifier_. A continuación se muestra cómo usar la herramienta. Primero se ejecuta el comando `hash-identifier` y después se introduce la _hash_ obtenida con el exploit de _sql injection_, así es como obtenemos el posible algoritmo de _hash()_.

![hash-identifier](/images/blog/pCix/simpleCTF/_resources/a1ce9a83343892794235e63bed632f74.png)

Cuando se cuenta con la _hash_ de una contraseña se puede intentar encontrar dicha contraseña. Es necesario uilizar una lista de contraseñas e iterar una por una comparando el resultado de la función _hash()_  y en caso de que coincidan se puede decir que se ha encontrado la contraseña ya que dicha contraseña genera la  _hash_ con la que se está comparando.

Recordemos que en un inicio encontramos un mensaje que nos sugería que la contraseña era fácil de crackear, por lo que muy probable que podramos crackear la contraseña con una lista de contraseñas comunes. En Kali Linux se cuenta con un archivo llamado _rockyou.txt.gz_ ubicado en la carpeta _/usr/share/wordlists_ y se puede descomprimir con el comando `gunzip rockyou.txt.gz `. Cuando descomprimimos dicho archivo podemos obtener un archivo `.txt` que cuenta con las contraseñas más usadas y con ayuda de este podemos intentar encontrar la contraseña que nos permitirá acceder al sistema.

En este caso también debemos considerar que la _hash_ de la contraseña incluye un _salt_ que es un valor que se suma a la entrada (contraseña)  y a este par se le aplica la función _hash_, esto se hace con la finalidad de que una contraseña no sea tan sencilla de crackear, ya que por muy simple que sea la contraseña se debe de encontrar la manera de probar cada contraseña con la _salt_ correspondiente.

Se utilizará la herramienta _hashcat_ para crackear la contraseña. Es necesario que se especifique el tipo de algoritmo _hash_, la _hash_ que obtuvimos del _sql injection_, y la _salt_ que también obtuvimos del _sql injection_.

Para intentar encontrar la contraseña se utilizó el siguiente comando:

![hashcat](/images/blog/pCix/simpleCTF/_resources/fbbb67c4c30309c8f9ebb77713c4eabb.png)

En este caso realizaremos un _wordlist attack_ que consiste en utilizar una lista/diccionario de constraseñas e iterar una por una hasta encontrar la contraseña que al aplicarle la función _hash_ de como resultado la _hash_ que recibe como entrada _hashcat_. Este modo de ataque es el correspondiente al _straight mode_ en _hashcat_ el cual se indica con la bandera (a) de la siguiente manera: `-a 0`, además se debe de especificar el *hash_type*, este será el número correspondiente al tipo de _hash_ que arrojó el _hash-identifier_. Por último, recordemos que contamos con una _salt_ y una _hash_, estos valores se escribirán entre comillas con el siguiente formato: `"hash:salt"`. Recordemos que tanto la _hash_ como la _salt_ fueron obtenidas previamente con el _sql injection_.

![hashcatResultado](/images/blog/pCix/simpleCTF/_resources/276d98a761724283732c94b3f88657ce.png)

### Obtener una _shell_
Una vez que obtuvimos la contraseña intentamos entrar por _ssh_ con las credenciales que hemos encontrado hasta el momento. Debemos de tener en cuenta que el protocolo _ssh_ suele estar en el puerto 22 pero en este caso se encuentra en el 2222 por lo que harémos una ligera variación en el comando para realizar la conexión, se indicará con la bandera `-p` el puerto por el que se conectará de la siguiente manera:
`ssh -p 2222 mitch@10.10.108.27`
 Nota: en caso de recibir el mensaje "Are you sure you want to continue connecting (yes/no/[fingerprint])?" deberemos de introducir "yes" + enter.
 
Logramos obtener una shell, sin embargo por el _prompt_ (_$_) tan simple y que no podemos utilizar las flechas del teclado, nos damos cuenta que contamos con una _shell_ _sh_ por lo que intentamos utilizar _/bin/bash_ para obtener una _bash_ que es una versión mejorada de _sh_.

 ![ssh](/images/blog/pCix/simpleCTF/_resources/f853ce53a26ef12011b0f2512146b700.png)
 
 Ya que contamos con una _bash_ incluso vemos mejor el _prompt_ (mitch@Machine:~$ ). 
 Primero probamos el comando: `whoami` para saber quiénes somos:

 ![whoami](/images/blog/pCix/simpleCTF/_resources/61cff124cf1f3c2fdd3f2f64fb1b16c7.png)

Si vemos los archivos que están en el directorio actual nos damos cuenta que tenemos la primera bandera:

![bandera1](/images/blog/pCix/simpleCTF/_resources/ae3551374e2e610430db32d71fffd947.png)

### Escalamiento de privilegios
Aunque tengamos una _shell_ eso no significa que tenemos permisos de superusuario, es por eso que proseguimos a encontrar una manera de obtener estos los privilegios. Ejecutamos el comando: `sudo -l` para saber qué es lo que podemos ejecutar como superusuario:

![sudo-l](/images/blog/pCix/simpleCTF/_resources/5457cbc71e18902f2316676f2069e371.png)

Ahora que sabemos que podemos ejecutar sin contraseña y como superusuario _vim_ podemos buscar como hacer el escalamiento de privilegios con _vim_. Una opción es ejecutar _vim_ indicandole un comando a ejecutar y conseguir una _shell_ con privilegios de superusuario de la siguiente manera:

![escalamiento](/images/blog/pCix/simpleCTF/_resources/aca11ef94b8e089ec1f84607f9a9a5f5.png)

Como primer intento se trató de conseguir una _bash_ sin embargo no obtuvimos ninguna shell por lo que se intentó con  una _sh_.

Algunas veces resulta incómodo trabajar con _shells_ _sh_ por lo que intentamos obtener una _bash_ con _python3_ :

![pty](/images/blog/pCix/simpleCTF/_resources/e0ae3f9ae30ef46adb590fc89f52645d.png)

Finalmente obtenemos la última bandera que se encontraba en la carpeta `/root`:

![bandera2](/images/blog/pCix/simpleCTF/_resources/2ad57937641edd836f6d2eefcc352149.png)


